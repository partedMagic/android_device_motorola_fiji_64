$(call inhernit-product, vendor/lineage/commong_full_phone.mk)

PRODUCT_NAME := lineage_fiji_64
PRODUCT_DEVICE := fiji_64
PRODUCT_BRAND := motorola
PRODUCT_MODEL := moto e6s
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

ifneq ($(SIGN_BUILD),true)
PRODUCT_BUILD_PROP_OVERRIDES += \
	BUILD_FINGERPRINT=motorola/fiji_64/fiji:9/POES29.288-60-6-1-29/c6fde:user/release-keys \
	PRIVATE_BUILD_DESC="fiji_64-user 9 POES29.288-60-6-1-29 c6fde release-keys"
endif
